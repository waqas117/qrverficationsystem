///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.qrcode.encrypt.utils;
//
//import java.security.InvalidKeyException;
//import java.security.KeyFactory;
//import java.security.NoSuchAlgorithmException;
//import java.security.NoSuchProviderException;
//import java.security.PrivateKey;
//import java.security.Signature;
//import java.security.SignatureException;
//import java.security.spec.PKCS8EncodedKeySpec;
//import java.security.spec.X509EncodedKeySpec;
//import org.apache.commons.io.IOUtils;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.core.io.Resource;
//import org.springframework.core.io.ResourceLoader;
//import org.springframework.stereotype.Component;
//
///**
// *
// * @author Atif
// */
//@Component
//public class DigitalSignature {
//
//    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(DigitalSignature.class);
//
//    @Autowired
//    private ResourceLoader resourceLoader;
//
//    // https://docs.oracle.com/javase/8/docs/api/java/security/spec/PKCS8EncodedKeySpec.html
//    public PrivateKey getPrivateKey(String filename) throws Exception {
//        Resource resource = resourceLoader.getResource("classpath:" + filename);
//        byte[] privateKeyEncoded = IOUtils.toByteArray(resource.getInputStream());
//        PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(privateKeyEncoded);
//        KeyFactory keyFactory = KeyFactory.getInstance("DSA", "SUN");
//        return keyFactory.generatePrivate(privateKeySpec);
//    }
//
//    public byte[] getDigitalSignature(String privateKey, String data) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, SignatureException, Exception {
//        // Get an instance of Signature object and initialize it.
//        Signature signature = Signature.getInstance("SHA1withDSA", "SUN");
//        signature.initSign(getPrivateKey(privateKey));
//        // Supply the data to be signed to the Signature object
//        // using the update() method and generate the digital
//        // signature.
//        byte[] bytes = data.getBytes();
//        signature.update(bytes);
//        byte[] digitalSignature = signature.sign();
//        byte[] combined = new byte[digitalSignature.length + bytes.length];
//        System.arraycopy(digitalSignature, 0, combined, 0, digitalSignature.length);
//        System.arraycopy(bytes, 0, combined, digitalSignature.length, bytes.length);
//        return combined;
//    }
//
//    public byte[] getDigitalSignature(String privateKey, byte[] data) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, SignatureException, Exception {
//        // Get an instance of Signature object and initialize it.
//        Signature signature = Signature.getInstance("SHA1withDSA", "SUN");
//        signature.initSign(getPrivateKey(privateKey));
//        // Supply the data to be signed to the Signature object
//        // using the update() method and generate the digital
//        // signature.
//        byte[] bytes = data;//.getBytes();
//        signature.update(bytes);
//        byte[] digitalSignature = signature.sign();
//        logger.info("Digital Signature Length: {}, Object Bytes Length: {}", digitalSignature.length, bytes.length);
//        byte[] combined = new byte[digitalSignature.length + bytes.length];
//        System.arraycopy(digitalSignature, 0, combined, 0, digitalSignature.length);
//        System.arraycopy(bytes, 0, combined, digitalSignature.length, bytes.length);
//        return combined;
//    }
//}
