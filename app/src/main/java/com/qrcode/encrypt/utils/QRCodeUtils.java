///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.qrcode.encrypt.utils;
//
//import com.google.gson.Gson;
//import com.google.gson.GsonBuilder;
//import com.google.zxing.EncodeHintType;
//import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
//import com.posiscan.qrcode.web.resource.CertificateResource;
//import java.io.BufferedReader;
//import java.io.ByteArrayInputStream;
//import java.io.ByteArrayOutputStream;
//import java.io.IOException;
//import java.io.InputStreamReader;
//import java.io.ObjectOutputStream;
//import java.nio.charset.StandardCharsets;
//import java.security.InvalidKeyException;
//import java.security.NoSuchAlgorithmException;
//import java.security.NoSuchProviderException;
//import java.security.PrivateKey;
//import java.security.Signature;
//import java.security.SignatureException;
//import java.util.Base64;
//import java.util.Date;
//import java.util.EnumMap;
//import java.util.Map;
//import java.util.zip.GZIPInputStream;
//import java.util.zip.GZIPOutputStream;
//import org.apache.commons.io.IOUtils;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
///**
// *
// * @author Atif
// */
//@Component
//public class QRCodeUtils {
//
//    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(QRCodeUtils.class);
//
//    @Autowired
//    AsymmetricCryptography asymmetricCryptography;
//
//    public final int width = 256;
//    public final int height = 256;
//    public int white = 0xFFFFFFFF;//255 << 16 | 255 << 8 | 255;
//    public int black = 0xFF000000;
//
//    public Map<EncodeHintType, Object> getHintTypeMap() {
//        Map<EncodeHintType, Object> hintMap = new EnumMap<>(EncodeHintType.class);
//        hintMap.put(EncodeHintType.CHARACTER_SET, "UTF-8");
//        hintMap.put(EncodeHintType.MARGIN, 1); // default = 4, Now with zxing version 3.2.1 you could change border size (white border size to just 1)
//        hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
//        return hintMap;
//    }
//
//    public byte[] getDigitalSignature(String privateKey, String data) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, SignatureException, Exception {
//        // Get an instance of Signature object and initialize it.
//        Signature signature = Signature.getInstance("SHA1withDSA", "SUN");
//        signature.initSign(asymmetricCryptography.getPrivate(privateKey));
//        // Supply the data to be signed to the Signature object
//        // using the update() method and generate the digital
//        // signature.
//        byte[] bytes = data.getBytes();
//        signature.update(bytes);
//        byte[] digitalSignature = signature.sign();
//        return digitalSignature;
//    }
//
//    public String getDigitalSignature1(PrivateKey privateKey, String data) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, SignatureException {
//        // Get an instance of Signature object and initialize it.
//        Signature signature = Signature.getInstance("SHA1withDSA", "SUN");
//        signature.initSign(privateKey);
//        // Supply the data to be signed to the Signature object
//        // using the update() method and generate the digital
//        // signature.
//        byte[] bytes = data.getBytes();
//        signature.update(bytes);
//        byte[] digitalSignature = signature.sign();
//        int size = digitalSignature.length;
//        byte[] total = new byte[1 + digitalSignature.length + bytes.length];
//        return null;
//    }
//
//    public String getCompressedBase64String(String normalString) throws IOException {
//        byte[] compressStringByteArray = compress(normalString);
//        return Base64.getEncoder().encodeToString(compressStringByteArray);
//    }
//
//    public String getCompressedBase64String(byte[] normalString) throws IOException {
//        byte[] compressStringByteArray = compress(normalString);
//        logger.info("compressStringByteArray Size: " + compressStringByteArray.length);
//        return Base64.getEncoder().encodeToString(compressStringByteArray);
//    }
//
//    public byte[] compress(String data) throws IOException {
//        ByteArrayOutputStream bos = new ByteArrayOutputStream(data.length());
//        GZIPOutputStream gzip = new GZIPOutputStream(bos);
//        gzip.write(data.getBytes());
//        gzip.close();
//        byte[] compressed = bos.toByteArray();
//        bos.close();
//        return compressed;
//    }
//
//    public static byte[] compress(byte[] data) throws IOException {
//        ByteArrayOutputStream bos = new ByteArrayOutputStream(data.length);
//        GZIPOutputStream gzip = new GZIPOutputStream(bos);
//        gzip.write(data);
//        gzip.close();
//        byte[] compressed = bos.toByteArray();
//        bos.close();
//        return compressed;
//    }
//
//    public static String decompress(byte[] compressed) throws IOException {
//        ByteArrayInputStream bis = new ByteArrayInputStream(compressed);
//        GZIPInputStream gis = new GZIPInputStream(bis);
//        BufferedReader br = new BufferedReader(new InputStreamReader(gis, "UTF-8"));
//        StringBuilder sb = new StringBuilder();
//        String line;
//        while ((line = br.readLine()) != null) {
//            sb.append(line);
//        }
//        br.close();
//        gis.close();
//        bis.close();
//        return sb.toString();
//    }
//
//    public static String decompressModified(final byte[] compressed) throws IOException {
//        ByteArrayInputStream bis = new ByteArrayInputStream(compressed);
//        GZIPInputStream gis = new GZIPInputStream(bis);
//        byte[] bytes = IOUtils.toByteArray(gis);
//        return new String(bytes, StandardCharsets.UTF_8);
//    }
//
//    public Gson getGson() {
//        final GsonBuilder gsonBuilder = new GsonBuilder();
//        gsonBuilder.serializeNulls().setPrettyPrinting();
//        return gsonBuilder.create();
//    }
//}
