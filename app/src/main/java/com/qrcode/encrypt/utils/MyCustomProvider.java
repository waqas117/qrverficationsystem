package com.qrcode.encrypt.utils;

import java.security.Provider;

public class MyCustomProvider  extends Provider{
    /**
     * Constructs a provider with the specified name, version number,
     * and information.
     *
     */
    public MyCustomProvider() {
        super("Crypto", 1.0, "HARMONY (SHA1 digest; SecureRandom; SHA1withDSA signature)");
        put("SecureRandom.SHA1PRNG",
                "org.apache.harmony.security.provider.crypto.SHA1PRNG_SecureRandomImpl");
        put("SecureRandom.SHA1PRNG ImplementedIn", "Software");
    }
}
