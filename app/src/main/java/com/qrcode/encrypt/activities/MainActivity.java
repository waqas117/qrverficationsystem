package com.qrcode.encrypt.activities;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.qrcode.encrypt.R;
import com.qrcode.encrypt.model.DocumentSubType;
import com.qrcode.encrypt.model.DocumentType;
import com.qrcode.encrypt.model.Doc;
import com.qrcode.encrypt.model.nadra.BirthCertificateModel;
import com.qrcode.encrypt.model.uae.TitleDeedModel;
import com.qrcode.encrypt.model.uae.UaeBirthCertificateModel;
import com.qrcode.encrypt.model.university.UniversityTranscript;
import com.qrcode.encrypt.model.QRCodeInfo;
import com.qrcode.encrypt.model.university.UniversityDegree;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.ByteBuffer;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.X509EncodedKeySpec;
import java.util.zip.GZIPInputStream;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    private Button button;


//    static {
//        try {
//            SecureRandom sr = SecureRandom.getInstance("SHA1PRNG", new MyCustomProvider());
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        }
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button = findViewById(R.id.scan);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        TextView pressScanTextView = findViewById(R.id.press_scantextView);

        Animation shake;
        shake = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake);

        shake.setRepeatCount(Animation.INFINITE);

        pressScanTextView.setAnimation(shake);

        Log.i(TAG , "created mainactivity");



        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                IntentIntegrator integrator = new IntentIntegrator(MainActivity.this);
//                integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
                integrator.setPrompt("Scan QR Code");
                integrator.setOrientationLocked(false);
//                integrator.setCameraId(0);
//                integrator.setBeepEnabled(false);
//                integrator.setBarcodeImageEnabled(false);
                integrator.initiateScan();

            }
        });
    }

    public  String convertStreamToString(InputStream is)
            throws IOException {
        Writer writer = new StringWriter();

        char[] buffer = new char[2048];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(is,
                    "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
        } finally {
            is.close();
        }
        String text = writer.toString();
        return text;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                Log.i(TAG, "Cancelled scan");
                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
            }else {



                //this regx is just wrote becuase qr scanner library has a bug whenever signedData throws exception it recreated the
                //Activity so stop to recreation we check either this is normal QR or Our Encoded QR
                boolean isAscii = result.getContents().matches("\\A\\p{ASCII}*\\z");

                if (isAscii){

                    Log.i(TAG , "This is normal QR Process it");

                    Intent intent = new Intent(this , ErrorActivity.class);

                    intent.putExtra(ErrorActivity.getERROR_TEST() , result.getContents());

                    startActivity(intent);

                    return;
                }



                Log.i(TAG , isAscii+" Raw bytes"+result.getContents());



                byte[] signedData = new byte[0];

                byte[] dataArray = new byte[0];
                byte[] signatureArray = new byte[0];
                try {
                    byte[] realData = getRealData(result.getRawBytes());
//
                    signedData = decompressModified2(realData) ;

//                    Log.i(TAG , "signedData Length: "+ signedData.signatureLength);

                    byte[] lengthByte = new byte[]{signedData[0] , signedData[1] , signedData[2] , signedData[3]};
                    int signatureLength = fromByteArray(lengthByte);

                    int srcPosition = signatureLength + 4 ;
                    int dataElements = signedData.length - srcPosition ;
                     dataArray = new byte[dataElements];

                    System.arraycopy(signedData , srcPosition , dataArray , 0 , dataElements);

                    signatureArray = new byte[signatureLength];
                    System.arraycopy(signedData , 4 , signatureArray , 0 , signatureLength );

                    Log.i(TAG  , "signature Array length:"+signatureArray.length);

                    Log.i(TAG , "signature array:"+bytesToHex(signatureArray));


                } catch (ArrayIndexOutOfBoundsException|IOException e) {
//                    e.printStackTrace();
//                    unVerifiedData("InValid QR Try again");
                    return;
                }

//



                boolean verify ;
                try {
                     verify= verifySignature(dataArray , signatureArray);

                     if (verify){
                         Log.i(TAG , "verified data ->");

                         String dataString = new String(dataArray  , "UTF-8");

                         Log.i(TAG  , "data:"+dataString);

                         extractJson(dataString);


                     }else{
                         Log.i(TAG ,  "verification failed");
                         unVerifiedData("Verification Failed");

                     }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                }
            }


        }

        private void unVerifiedData(String msg){
            Snackbar snackBar = Snackbar.make(findViewById(R.id.main_container) , msg,Snackbar.LENGTH_LONG);

            snackBar.getView().setBackgroundColor(Color.RED);
            snackBar.show();
        }

    //Method for signature verification that initializes with the Public Key,
    //updates the data to be verified and then verifies them using the signature
    private boolean verifySignature(byte[] data, byte[] signature) throws Exception {
        Signature sig = Signature.getInstance("SHA1withDSA");
        PublicKey publicKey =  getPublic();

        Log.i(TAG , "publickey"+publicKey.getAlgorithm());
        Log.i(TAG , "publickey format"+publicKey.getFormat());
        sig.initVerify(publicKey);
        sig.update(data);

        return sig.verify(signature);
    }

    //Method to retrieve the Public Key from a Asset
    public PublicKey getPublic() throws Exception {
        InputStream publicStream = getResources().getAssets().open("publicKey");
//        byte[] keyBytes = new byte[publicStream.available()];
//
//        publicStream.read(keyBytes);

        byte[] keyBytes = getBytes(publicStream);

        String hex = bytesToHex(keyBytes);

        Log.i(TAG , "hexpublickey:"+hex);

        Log.i(TAG , "public key length:"+keyBytes.length);

        X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);

        KeyFactory kf = KeyFactory.getInstance("DSA");
        return kf.generatePublic(spec);
    }

    public static byte[] getBytes(InputStream is) throws IOException {

        int len;
        int size = 1024;
        byte[] buf;

        if (is instanceof ByteArrayInputStream) {
            size = is.available();
            buf = new byte[size];
            len = is.read(buf, 0, size);
        } else {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            buf = new byte[size];
            while ((len = is.read(buf, 0, size)) != -1)
                bos.write(buf, 0, len);
            buf = bos.toByteArray();
        }
        return buf;
    }

        public static String decompress ( byte[] compressed) throws IOException {
            ByteArrayInputStream bis = new ByteArrayInputStream(compressed);
            GZIPInputStream gis = new GZIPInputStream(bis);
            BufferedReader br = new BufferedReader(new InputStreamReader(gis, "UTF-8"));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            br.close();
            gis.close();
            bis.close();
            return sb.toString();

//            return "{\"universityName\":\"University of Education\",\"address\":\"Township Lahore\",\"documentType\":\"Degree\",\"programName\":\"BSIT\",\"programSession\":\"Fall 2009-2013\",\"candidateName\":\"Hafiz Atif Jamil\",\"rollNumber\":\"A234\",\"fatherName\":\"Jamil Ahmad\",\"registrationNumber\":123456789,\"comprehensiveExamNotificationNumber\":123456,\"comprehensiveExamNotificationDate\":\"15-04-2013\",\"comprehensiveExamRollNumber\":789789,\"comprehensiveExamResult\":\"Pass\",\"totalMarks\":1020,\"obtainMarks\":1100,\"cgpa\":3.67,\"comulativeGrade\":\"B\",\"percentage\":78,\"issueDate\":\"28-06-2014\",\"qrCodeImageUrl\":null}";


        }

    public byte[] decompressModified2(final byte[] compressed) throws IOException {
        ByteArrayInputStream bis = new ByteArrayInputStream(compressed);
        GZIPInputStream gis = new GZIPInputStream(bis);
//        byte[] bytes = IOUtils.toByteArray(gis);
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        int nRead;
        byte[] data = new byte[1024];
        while ((nRead = gis.read(data, 0, data.length)) != -1) {
            buffer.write(data, 0, nRead);
        }

        buffer.flush();
        byte[] byteArray = buffer.toByteArray();
//        Log.i(TAG , "decompress Modified size: "+ byteArray.length);
        return byteArray;
    }

//    public String decompressModified(final byte[] compressed) throws IOException {
//        ByteArrayInputStream bis = new ByteArrayInputStream(compressed);
//        GZIPInputStream gis = new GZIPInputStream(bis);
//        byte[] bytes = IOUtils.toByteArray(gis);
//        String decompressString = new String(bytes, "UTF-8");
//        return decompressString;
//    }

        private void extractJson(String jsonData){

//            Toast.makeText(this, "data: " + jsonData, Toast.LENGTH_LONG).show();
            QRCodeInfo qrCodeInfo = new Gson().fromJson(jsonData  , QRCodeInfo.class);
            Log.i(TAG , qrCodeInfo.getDocumentSubType()+"");

                int documentType = qrCodeInfo.getDocumentType();

                int documentSubType = qrCodeInfo.getDocumentSubType();

                if (documentType == DocumentType.UNIVERSITY_DOC.getCode()){

                    Intent intent = new Intent(MainActivity.this , UniversityActivity.class);
                    Log.i(TAG , "Actuall Data:"+qrCodeInfo.getData());

                    JsonObject jsonObject =  qrCodeInfo.getData();
                    Doc doc = null;
                    if (documentSubType == DocumentSubType.UNIVERSITY_DEGREE.getCode()){
                        doc = new Gson().fromJson(jsonObject.toString() , UniversityDegree.class);

                        Log.i(TAG , "university transcript");

                    }else if (documentSubType == DocumentSubType.UNIVERSIT_TRANSCRIPT.getCode()){
                        doc = new Gson().fromJson(jsonObject.toString() , UniversityTranscript.class);
                        Log.i(TAG , "university degree");

                    }
                    intent.putExtra(UniversityActivity.getUNIVERSITY_CARD() , doc);
                    startActivity(intent);
                }else if (documentType == DocumentType.NADRA_DOC.getCode()){

                    Intent intent = new Intent(MainActivity.this , NadraReportActivity.class);
                    Log.i(TAG , "birth Data:"+qrCodeInfo.getData());

                    JsonObject jsonObject =  qrCodeInfo.getData();

                    Doc doc = null ;


                    if (documentSubType == DocumentSubType.NADRA_BIRTH_CERTIFICATE.getCode()){

                        doc = new Gson().fromJson(jsonObject.toString() , BirthCertificateModel.class);
                    }else {
                        return;
                    }

                    intent.putExtra(NadraReportActivity.getNADRA_CARD() , doc);

                    startActivity(intent);



                }else if (documentType == DocumentType.UAE_DOC.getCode()){

                    Intent intent = new Intent(MainActivity.this , UaeReportActivity.class);
                    Log.i(TAG , "uae birth Data:"+qrCodeInfo.getData());

                    JsonObject jsonObject =  qrCodeInfo.getData();

                    Doc doc = null ;


                    if (documentSubType == DocumentSubType.UAE_BIRTH_CERTIFICATE.getCode()){

                        doc = new Gson().fromJson(jsonObject.toString() , UaeBirthCertificateModel.class);
                    }else if (documentSubType == DocumentSubType.UAE_TITLE_DEED.getCode()){


                        doc = new Gson().fromJson(jsonObject.toString() , TitleDeedModel.class);

                    }

                    intent.putExtra(UaeReportActivity.getUAE_CARD() , doc);

                    startActivity(intent);



                }



        }



    public static String bytesToHex(byte[] bytes) {
        char[] hexArray = "0123456789ABCDEF".toCharArray();
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    int fromByteArray(byte[] bytes) {
        return ByteBuffer.wrap(bytes).getInt();
    }

    public  byte[] getRealData(byte[] raw) throws ArrayIndexOutOfBoundsException {

        if (raw == null) Toast.makeText(getApplicationContext(), "Please Scan Again", Toast.LENGTH_SHORT).show();;
        if (raw[0] != 0x40) Toast.makeText(getApplicationContext(), "Please Scan correct image of QR", Toast.LENGTH_SHORT).show();;
        if (raw.length < 4) throw new IllegalArgumentException("array too short");

        int dataLength = (raw[1] << 4) | ((raw[2] & 0xF0) >>> 4);

        byte[] processed = new byte[dataLength];
        for (int i = 0; i < dataLength; i++) {
            processed[i] = (byte) ((raw[i + 2] << 4) | ((raw[i + 3] & 0xF0) >>> 4));
        }
        return processed;
    }


    }

