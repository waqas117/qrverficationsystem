package com.qrcode.encrypt.fragments.university

import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ImageView
import android.widget.TextView

import com.qrcode.encrypt.R
import com.qrcode.encrypt.activities.UniversityActivity
import com.qrcode.encrypt.model.university.UniversityDegree
import com.qrcode.encrypt.model.university.UniversityTranscript

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [Degree.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [Degree.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class Degree : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_web ,container , false);
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var degree = arguments?.getParcelable<UniversityDegree>(UniversityActivity.UNIVERSITY_CARD) as UniversityDegree

        var webView :WebView = view.findViewById<WebView>(R.id.webview) as WebView

        webView.getSettings().setJavaScriptEnabled(true)
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(true);
        webView.setInitialScale(1);

        webView.addJavascriptInterface(degree, "universityDegree")
        webView.webViewClient =  WebViewClient()

        webView.loadUrl("file:///android_asset/qr_templates/degree.html")

//        showSnackResult(webView)

    }

    private fun showSnackResult(view:View) {

        // Create the Snackbar
        val snackbar = Snackbar.make(view, "", Snackbar.LENGTH_INDEFINITE)
// Get the Snackbar's layout view
        val layout = snackbar.view as Snackbar.SnackbarLayout
// Hide the text
        val textView = layout.findViewById<View>(android.support.design.R.id.snackbar_text) as TextView
        textView.visibility = View.INVISIBLE

// Inflate our custom view
        val snackView = layoutInflater.inflate(R.layout.verfiy_snackbar, null)


        val textViewTop = snackView.findViewById(R.id.snack_text) as TextView

        textViewTop.setTextColor(Color.WHITE)

//If the view is not covering the whole snackbar layout, add this line
        layout.setPadding(0, 0, 0, 0)

// Add the view to the Snackbar's layout
        layout.addView(snackView, 0)
        context?.let { ContextCompat.getColor(it, R.color.snackGreen) }?.let { snackbar.view.setBackgroundColor(it) }
//
// Show the Snackbar
        snackbar.show()


    }

}
