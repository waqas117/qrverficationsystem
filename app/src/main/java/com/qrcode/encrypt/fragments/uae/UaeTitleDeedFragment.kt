package com.qrcode.encrypt.fragments.uae

import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.text.style.StyleSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.TextView
import com.qrcode.encrypt.R
import com.qrcode.encrypt.activities.UaeReportActivity
import com.qrcode.encrypt.model.uae.TitleDeedModel
import com.qrcode.encrypt.model.uae.UaeBirthCertificateModel
import com.qrcode.encrypt.utils.SimpleSpanBuilder
import kotlinx.android.synthetic.main.fragment_web.*

class UaeTitleDeedFragment: Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {


        return inflater.inflate(R.layout.fragment_web, container, false);
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var webView: WebView = view.findViewById<WebView>(R.id.webview) as WebView

        webView.getSettings().setJavaScriptEnabled(true)
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(true);
        webView.setInitialScale(1);

        var certificate: TitleDeedModel? = arguments?.getParcelable<TitleDeedModel>(UaeReportActivity.UAE_CARD) as TitleDeedModel


        webView.addJavascriptInterface(certificate, "titleDeed")
        webView.webViewClient = WebViewClient()

        webView.loadUrl("file:///android_asset/qr_templates/uae/title_deed/titleDeed.html")

        val bottomToTop = AnimationUtils.loadAnimation(context?.applicationContext, R.anim.bottom_to_original)

        var simpleSpan = SimpleSpanBuilder().appendWithLineBreak("Geninue Document", StyleSpan(Typeface.BOLD))
                .appendWithLineBreak("Validated by a Valid Government")
                .append("of Dubai Digital Signature")

        verify_text.text = simpleSpan.build()

        verify_text.animation = bottomToTop

//        showSnackResult(view)
    }

    private fun showSnackResult(view: View) {

        // Create the Snackbar
        val snackbar = Snackbar.make(view, "", Snackbar.LENGTH_INDEFINITE)
// Get the Snackbar's layout view
        val layout = snackbar.view as Snackbar.SnackbarLayout
// Hide the text
        val textView = layout.findViewById<View>(android.support.design.R.id.snackbar_text) as TextView
        textView.visibility = View.INVISIBLE

// Inflate our custom view
        val snackView = layoutInflater.inflate(R.layout.verfiy_snackbar, null)


        val textViewTop = snackView.findViewById(R.id.snack_text) as TextView

        textViewTop.setTextColor(Color.WHITE)

//If the view is not covering the whole snackbar layout, add this line
        layout.setPadding(0, 0, 0, 0)

// Add the view to the Snackbar's layout
        layout.addView(snackView, 0)
        context?.let { ContextCompat.getColor(it, R.color.snackGreen) }?.let { snackbar.view.setBackgroundColor(it) }
//
// Show the Snackbar
        snackbar.show()


    }

}