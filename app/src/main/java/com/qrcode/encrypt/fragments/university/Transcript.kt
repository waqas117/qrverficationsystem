package com.qrcode.encrypt.fragments.university

import android.os.Bundle
import android.support.design.widget.TextInputEditText
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

import com.qrcode.encrypt.R
import com.qrcode.encrypt.activities.UniversityActivity
import com.qrcode.encrypt.model.university.UniversityTranscript


class Transcript : Fragment() {

    private var mUniName: TextView? = null
    private var mDocumentType:TextView? = null
    private var mProgramName: TextInputEditText? = null
    private var mExamNotifDate:TextInputEditText? = null
    private var mProgramSession:TextInputEditText? = null
    private var mExamRollNo:TextInputEditText? = null
    private var mName: TextInputEditText? = null
    private var mExamResult:TextInputEditText? = null
    private var mFatherName:TextInputEditText? = null
    private var mTotalMarks:TextInputEditText? = null
    private var mRegNumber:TextInputEditText? = null
    private var mObtainMarks:TextInputEditText? = null
    private var mRollNo:TextInputEditText? = null
    private var mCGPA:TextInputEditText? = null
    private var mAddress:TextInputEditText? = null
    private var mGrade:TextInputEditText? = null
    private var mPrecentage: TextInputEditText? = null
    private var mIssueDate:TextInputEditText? = null

    private var qrImg: ImageView? = null



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_uni_transcript, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mUniName = view.findViewById<TextView>(R.id.uni_name)
        mDocumentType = view.findViewById<TextView>(R.id.document_type)

        mProgramName = view.findViewById<TextInputEditText>(R.id.program_name_textview)
        mExamNotifDate = view.findViewById<TextInputEditText>(R.id.exam_date_textview)
        mProgramSession = view.findViewById<TextInputEditText>(R.id.program_session_textview)
        mExamRollNo = view.findViewById<TextInputEditText>(R.id.exam_rollno_textview)

        mName = view.findViewById<TextInputEditText>(R.id.name_textview)
        mExamResult = view.findViewById<TextInputEditText>(R.id.exam_result_textview)
        mFatherName = view.findViewById<TextInputEditText>(R.id.father_textview)
        mTotalMarks = view.findViewById<TextInputEditText>(R.id.total_marks_textview)
        mRegNumber = view.findViewById<TextInputEditText>(R.id.registration_textview)
        mObtainMarks = view.findViewById<TextInputEditText>(R.id.obtain_textview)
        mRollNo = view.findViewById<TextInputEditText>(R.id.rollno_textview)
        mCGPA = view.findViewById<TextInputEditText>(R.id.cgpa_textview)
        mAddress = view.findViewById<TextInputEditText>(R.id.address_textview)
        mGrade = view.findViewById<TextInputEditText>(R.id.grade_textview)
        mPrecentage = view.findViewById<TextInputEditText>(R.id.percentage_textview)
        mIssueDate = view.findViewById<TextInputEditText>(R.id.issue_date_textview)

        qrImg = view.findViewById<ImageView>(R.id.qr_code)

        var degree = arguments?.getParcelable<UniversityTranscript>(UniversityActivity.UNIVERSITY_CARD)

        degree?.let { setAllUniFields(it) }
    }


    fun setAllUniFields(allUniFields: UniversityTranscript) {

        mUniName?.setText(allUniFields.universityName)
        mDocumentType?.setText(allUniFields.documentType)
        mProgramName?.setText(allUniFields.programName)
        mExamNotifDate?.setText(allUniFields.comprehensiveExamNotificationDate)
        mProgramSession?.setText(allUniFields.programSession)
        mExamRollNo?.setText(allUniFields.comprehensiveExamRollNumber)
        mName?.setText(allUniFields.candidateName)
        mExamResult?.setText(allUniFields.comprehensiveExamResult)
        mFatherName?.setText(allUniFields.fatherName)
        mTotalMarks?.setText(allUniFields.totalMarks)
        mRegNumber?.setText(allUniFields.registrationNumber)
        mObtainMarks?.setText(allUniFields.obtainMarks)
        mRollNo?.setText(allUniFields.rollNumber)
        mCGPA?.setText(allUniFields.cgpa)
        mAddress?.setText(allUniFields.address)
        mGrade?.setText(allUniFields.comulativeGrade)
        mPrecentage?.setText(allUniFields.percentage)
        mIssueDate?.setText(allUniFields.issueDate)





    }


}
