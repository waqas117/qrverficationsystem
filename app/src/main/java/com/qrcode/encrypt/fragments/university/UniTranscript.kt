package com.qrcode.encrypt.fragments.university

import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.TextView
import com.qrcode.encrypt.R
import com.qrcode.encrypt.activities.UniversityActivity
import com.qrcode.encrypt.jsaccessors.TranscriptAccessor
import com.qrcode.encrypt.model.university.UniversityTranscript
import kotlinx.android.synthetic.main.activity_show_view.*
import kotlinx.android.synthetic.main.fragment_web.*
import com.qrcode.encrypt.R.id.webview
import android.widget.FrameLayout



class UniTranscript : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {


        return inflater.inflate(R.layout.fragment_web ,container , false);
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var webView:WebView = view.findViewById<WebView>(R.id.webview) as WebView


        webView.getSettings().setJavaScriptEnabled(true)
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(true);

        webView.setInitialScale(1);


        var degree:UniversityTranscript? = arguments?.getParcelable<UniversityTranscript>(UniversityActivity.UNIVERSITY_CARD) as UniversityTranscript

        if (degree != null) {
            Log.i("courses",""+degree.getCoursesArray())
        }
        webView.addJavascriptInterface(degree, "universityTranscript")
        webView.webViewClient =  WebViewClient()

        webView.loadUrl("file:///android_asset/qr_templates/trans.html")

//        showSnackResult(frame_container)
    }

    private fun showSnackResult(view:View) {

        // Create the Snackbar
        val snackbar = Snackbar.make(view, "", Snackbar.LENGTH_INDEFINITE)
// Get the Snackbar's layout view
        val layout = snackbar.view as Snackbar.SnackbarLayout
// Hide the text
        val textView = layout.findViewById<View>(android.support.design.R.id.snackbar_text) as TextView
        textView.visibility = View.INVISIBLE

// Inflate our custom view
        val snackView = layoutInflater.inflate(R.layout.verfiy_snackbar, null)


        val textViewTop = snackView.findViewById(R.id.snack_text) as TextView

        textViewTop.setTextColor(Color.WHITE)

//If the view is not covering the whole snackbar layout, add this line
        layout.setPadding(0, 0, 0, 0)

// Add the view to the Snackbar's layout
        layout.addView(snackView, 0)
        context?.let { ContextCompat.getColor(it, R.color.snackGreen) }?.let { snackbar.view.setBackgroundColor(it) }
//
// Show the Snackbar
        snackbar.show()


    }
}