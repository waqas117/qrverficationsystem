package com.qrcode.encrypt.deserilizer;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

public class MyDeserializer implements JsonDeserializer<MyGernicClass> {



    @Override
    public MyGernicClass deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        if (json.isJsonObject() && json.getAsJsonObject().get("data").isJsonObject()) {

            return context.deserialize(json.getAsJsonObject().get("data") , MyGernicClass.class);

        }

        return null;

    }


}
