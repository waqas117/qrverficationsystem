package com.qrcode.encrypt.model.university;

import android.os.Bundle;
import android.os.Parcel;
import android.webkit.JavascriptInterface;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.qrcode.encrypt.activities.UniversityActivity;
import com.qrcode.encrypt.model.Doc;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Created by Adil khan on 5/7/2018.
 */

public class UniversityTranscript implements Doc {

        @SerializedName("universityName")
        @Expose
        private String universityName;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("documentType")
        @Expose
        private String documentType;
        @SerializedName("programName")
        @Expose
        private String programName;
        @SerializedName("programSession")
        @Expose
        private String programSession;
        @SerializedName("candidateName")
        @Expose
        private String candidateName;
        @SerializedName("rollNumber")
        @Expose
        private String rollNumber;
        @SerializedName("fatherName")
        @Expose
        private String fatherName;
        @SerializedName("registrationNumber")
        @Expose
        private String registrationNumber;
        @SerializedName("comprehensiveExamNotificationNumber")
        @Expose
        private String comprehensiveExamNotificationNumber;
        @SerializedName("comprehensiveExamNotificationDate")
        @Expose
        private String comprehensiveExamNotificationDate;
        @SerializedName("comprehensiveExamRollNumber")
        @Expose
        private String comprehensiveExamRollNumber;
        @SerializedName("comprehensiveExamResult")
        @Expose
        private String comprehensiveExamResult;
        @SerializedName("totalMarks")
        @Expose
        private String totalMarks;
        @SerializedName("obtainMarks")
        @Expose
        private String obtainMarks;
        @SerializedName("cgpa")
        @Expose
        private String cgpa;
        @SerializedName("comulativeGrade")
        @Expose
        private String comulativeGrade;
        @SerializedName("percentage")
        @Expose
        private String percentage;
        @SerializedName("issueDate")
        @Expose
        private String issueDate;
        @SerializedName("qrCodeImageUrl")
        @Expose
        private String qrCodeImageUrl;

        @SerializedName("courses")
        @Expose
        private  Course [] courses ;

        @JavascriptInterface
        public Course[] getCourses() {
        return courses;
        }

        @JavascriptInterface
        public String getCoursesArray(){

            StringBuilder stringBuilder = new StringBuilder();

            for (Course course: courses) {

                stringBuilder.append(course.getCourseCode()).append(",");
                stringBuilder.append(course.getName()).append(",");
                stringBuilder.append(course.getCreditHours()).append(",");
                stringBuilder.append(course.getMaxMarks()).append(",");
                stringBuilder.append(course.getObtMarks()).append(",");
                stringBuilder.append(course.getCgpa()).append(",");
                stringBuilder.append(course.getGrade()).append("|");
            }

            String result = stringBuilder.toString();

           return  result.substring(0 , result.length()-1);


        }

    public void setCourses(Course[] courses) {
        this.courses = courses;
    }

    @JavascriptInterface
        public String getUniversityName() {
            return universityName;
        }

        public void setUniversityName(String universityName) {
            this.universityName = universityName;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getDocumentType() {
            return documentType;
        }

        public void setDocumentType(String documentType) {
            this.documentType = documentType;
        }

        @JavascriptInterface
        public String getProgramName() {
            return programName;
        }

        public void setProgramName(String programName) {
            this.programName = programName;
        }

        @JavascriptInterface
        public String getProgramSession() {
            return programSession;
        }

        public void setProgramSession(String programSession) {
            this.programSession = programSession;
        }

        @JavascriptInterface

        public String getCandidateName() {
            return candidateName;
        }

        public void setCandidateName(String candidateName) {
            this.candidateName = candidateName;
        }

        @JavascriptInterface

        public String getRollNumber() {
            return rollNumber;
        }

        public void setRollNumber(String rollNumber) {
            this.rollNumber = rollNumber;
        }

        @JavascriptInterface

        public String getFatherName() {
            return fatherName;
        }

        public void setFatherName(String fatherName) {
            this.fatherName = fatherName;
        }

        @JavascriptInterface

        public String getRegistrationNumber() {
            return registrationNumber+"";
        }

        public void setRegistrationNumber(String registrationNumber) {
            this.registrationNumber = registrationNumber;
        }

        @JavascriptInterface
        public String getComprehensiveExamNotificationNumber() {
            return comprehensiveExamNotificationNumber+"";
        }

        public void setComprehensiveExamNotificationNumber(String comprehensiveExamNotificationNumber) {
            this.comprehensiveExamNotificationNumber = comprehensiveExamNotificationNumber;
        }

        @JavascriptInterface
        public String getComprehensiveExamNotificationDate() {
            return comprehensiveExamNotificationDate;
        }

        public void setComprehensiveExamNotificationDate(String comprehensiveExamNotificationDate) {
            this.comprehensiveExamNotificationDate = comprehensiveExamNotificationDate;
        }

        @JavascriptInterface

        public String getComprehensiveExamRollNumber() {
            return comprehensiveExamRollNumber+"";
        }

        public void setComprehensiveExamRollNumber(String comprehensiveExamRollNumber) {
            this.comprehensiveExamRollNumber = comprehensiveExamRollNumber;
        }

        @JavascriptInterface
        public String getComprehensiveExamResult() {
            return comprehensiveExamResult;
        }

        public void setComprehensiveExamResult(String comprehensiveExamResult) {
            this.comprehensiveExamResult = comprehensiveExamResult;
        }

        @JavascriptInterface

        public String getTotalMarks() {
            return totalMarks;
        }

        public void setTotalMarks(String totalMarks) {
            this.totalMarks = totalMarks;
        }

        @JavascriptInterface

        public String getObtainMarks() {
            return obtainMarks;
        }

        public void setObtainMarks(String obtainMarks) {
            this.obtainMarks = obtainMarks;
        }

        @JavascriptInterface

        public String getCgpa() {
            return cgpa+"";
        }

        public void setCgpa(String cgpa) {
            this.cgpa = cgpa;
        }

        @JavascriptInterface
        public String getComulativeGrade() {
            return comulativeGrade;
        }

        public void setComulativeGrade(String comulativeGrade) {
            this.comulativeGrade = comulativeGrade;
        }

        @JavascriptInterface

        public String getPercentage() {
            return percentage;
        }

        public void setPercentage(String percentage) {
            this.percentage = percentage;
        }

        public String getIssueDate() {
            return issueDate;
        }

        public void setIssueDate(String issueDate) {
            this.issueDate = issueDate;
        }

        public String getQrCodeImageUrl() {
            return qrCodeImageUrl == null ? "http://www.hypergridbusiness.com/wp-content/uploads/2015/10/Sunnypeak-headset-basic-black-QR-code.jpg"
                    : qrCodeImageUrl;
        }

        public void setQrCodeImageUrl(String qrCodeImageUrl) {
            this.qrCodeImageUrl = qrCodeImageUrl;
        }

    @Override
    public String toString() {
        return "UniversityTranscript{" +
                "universityName='" + universityName + '\'' +
                ", address='" + address + '\'' +
                ", documentType='" + documentType + '\'' +
                ", programName='" + programName + '\'' +
                ", programSession='" + programSession + '\'' +
                ", candidateName='" + candidateName + '\'' +
                ", rollNumber='" + rollNumber + '\'' +
                ", fatherName='" + fatherName + '\'' +
                ", registrationNumber='" + registrationNumber + '\'' +
                ", comprehensiveExamNotificationNumber='" + comprehensiveExamNotificationNumber + '\'' +
                ", comprehensiveExamNotificationDate='" + comprehensiveExamNotificationDate + '\'' +
                ", comprehensiveExamRollNumber='" + comprehensiveExamRollNumber + '\'' +
                ", comprehensiveExamResult='" + comprehensiveExamResult + '\'' +
                ", totalMarks='" + totalMarks + '\'' +
                ", obtainMarks='" + obtainMarks + '\'' +
                ", cgpa='" + cgpa + '\'' +
                ", comulativeGrade='" + comulativeGrade + '\'' +
                ", percentage='" + percentage + '\'' +
                ", issueDate='" + issueDate + '\'' +
                ", qrCodeImageUrl='" + qrCodeImageUrl + '\'' +
                '}';
    }



    public UniversityTranscript() {
    }

    public byte[] getSerializeObject() throws IOException {
        //Serialization of object
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream out = new ObjectOutputStream(bos);
        out.writeObject(this);
        return bos.toByteArray();
    }

    public static UniversityTranscript getDeSerializeObject(byte[] object) throws IOException, ClassNotFoundException {
        //De-serialization of object
        ByteArrayInputStream bis = new ByteArrayInputStream(object);
        ObjectInputStream in = new ObjectInputStream(bis);
        UniversityTranscript universityDegree = (UniversityTranscript) in.readObject();
        return universityDegree;
    }

    @Override
    public Bundle getData() {

        Bundle bundle = new Bundle();

        bundle.putParcelable(UniversityActivity.getUNIVERSITY_CARD(), this);
        return bundle;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.universityName);
        dest.writeString(this.address);
        dest.writeString(this.documentType);
        dest.writeString(this.programName);
        dest.writeString(this.programSession);
        dest.writeString(this.candidateName);
        dest.writeString(this.rollNumber);
        dest.writeString(this.fatherName);
        dest.writeString(this.registrationNumber);
        dest.writeString(this.comprehensiveExamNotificationNumber);
        dest.writeString(this.comprehensiveExamNotificationDate);
        dest.writeString(this.comprehensiveExamRollNumber);
        dest.writeString(this.comprehensiveExamResult);
        dest.writeString(this.totalMarks);
        dest.writeString(this.obtainMarks);
        dest.writeString(this.cgpa);
        dest.writeString(this.comulativeGrade);
        dest.writeString(this.percentage);
        dest.writeString(this.issueDate);
        dest.writeString(this.qrCodeImageUrl);
        dest.writeTypedArray(this.courses, flags);
    }

    protected UniversityTranscript(Parcel in) {
        this.universityName = in.readString();
        this.address = in.readString();
        this.documentType = in.readString();
        this.programName = in.readString();
        this.programSession = in.readString();
        this.candidateName = in.readString();
        this.rollNumber = in.readString();
        this.fatherName = in.readString();
        this.registrationNumber = in.readString();
        this.comprehensiveExamNotificationNumber = in.readString();
        this.comprehensiveExamNotificationDate = in.readString();
        this.comprehensiveExamRollNumber = in.readString();
        this.comprehensiveExamResult = in.readString();
        this.totalMarks = in.readString();
        this.obtainMarks = in.readString();
        this.cgpa = in.readString();
        this.comulativeGrade = in.readString();
        this.percentage = in.readString();
        this.issueDate = in.readString();
        this.qrCodeImageUrl = in.readString();
        this.courses = in.createTypedArray(Course.CREATOR);
    }

    public static final Creator<UniversityTranscript> CREATOR = new Creator<UniversityTranscript>() {
        @Override
        public UniversityTranscript createFromParcel(Parcel source) {
            return new UniversityTranscript(source);
        }

        @Override
        public UniversityTranscript[] newArray(int size) {
            return new UniversityTranscript[size];
        }
    };
}
