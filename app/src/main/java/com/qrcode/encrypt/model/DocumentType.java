/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qrcode.encrypt.model;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Atif
 */
public enum DocumentType {

    UNIVERSITY_DOC(1),
    POLICE_DOC(2),
    BANK_DOC(3),
    TRAVEL_DOC(4),
    NADRA_DOC(5),
    UAE_DOC(6);

    private static final Map<Integer, DocumentType> documentTypeMap = new HashMap<>();

    static {
        for (DocumentType documentType : DocumentType.values()) {
            documentTypeMap.put(documentType.getCode(), documentType);
        }
    }

    private final int code;

    DocumentType(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public DocumentType toName(int code) {
        return documentTypeMap.get(code);
    }
}
