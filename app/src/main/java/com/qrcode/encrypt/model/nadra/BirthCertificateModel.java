package com.qrcode.encrypt.model.nadra;

import android.os.Bundle;
import android.os.Parcel;
import android.webkit.JavascriptInterface;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.qrcode.encrypt.activities.NadraReportActivity;
import com.qrcode.encrypt.model.Doc;

public class BirthCertificateModel implements Doc {


        @SerializedName("formNumber")
        @Expose
        private String formNumber;

        @SerializedName("crmsNumber")
        @Expose
        private String crmsNumber;
        @SerializedName("applicantName")
        @Expose
        private String applicantName;
        @SerializedName("applicantCnic")
        @Expose
        private String applicantCnic;
        @SerializedName("relation")
        @Expose
        private String relation;
        @SerializedName("childName")
        @Expose
        private String childName;
        @SerializedName("fatherName")
        @Expose
        private String fatherName;
        @SerializedName("fatherCnic")
        @Expose
        private String fatherCnic;
        @SerializedName("motherName")
        @Expose
        private String motherName;
        @SerializedName("motherCnic")
        @Expose
        private String motherCnic;
        @SerializedName("gendor")
        @Expose
        private String gendor;
        @SerializedName("religion")
        @Expose
        private String religion;
        @SerializedName("districtOfBirth")
        @Expose
        private String districtOfBirth;
        @SerializedName("dateOfBirth")
        @Expose
        private String dateOfBirth;
        @SerializedName("grandFatherName")
        @Expose
        private String grandFatherName;
        @SerializedName("grandFatherCnic")
        @Expose
        private String grandFatherCnic;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("applicantNameUrdu")
        @Expose
        private String applicantNameUrdu;
        @SerializedName("applicantCnicUrdu")
        @Expose
        private String applicantCnicUrdu;
        @SerializedName("relationUrdu")
        @Expose
        private String relationUrdu;
        @SerializedName("childNameUrdu")
        @Expose
        private String childNameUrdu;
        @SerializedName("fatherNameUrdu")
        @Expose
        private String fatherNameUrdu;
        @SerializedName("fatherCnicUrdu")
        @Expose
        private String fatherCnicUrdu;
        @SerializedName("motherNameUrdu")
        @Expose
        private String motherNameUrdu;
        @SerializedName("motherCnicUrdu")
        @Expose
        private String motherCnicUrdu;
        @SerializedName("gendorUrdu")
        @Expose
        private String gendorUrdu;
        @SerializedName("religionUrdu")
        @Expose
        private String religionUrdu;
        @SerializedName("districtOfBirthUrdu")
        @Expose
        private String districtOfBirthUrdu;
        @SerializedName("dateOfBirthUrdu")
        @Expose
        private String dateOfBirthUrdu;
        @SerializedName("grandFatherNameUrdu")
        @Expose
        private String grandFatherNameUrdu;
        @SerializedName("grandFatherCnicUrdu")
        @Expose
        private String grandFatherCnicUrdu;
        @SerializedName("addressUrdu")
        @Expose
        private String addressUrdu;
        @SerializedName("entryDate")
        @Expose
        private String entryDate;
        @SerializedName("entryType")
        @Expose
        private String entryType;
        @SerializedName("issueDateUrdu")
        @Expose
        private String issueDateUrdu;
        @SerializedName("imageUrl")
        @Expose
        private String imageUrl;

        @JavascriptInterface
        public String getFormNumber() {
            return formNumber;
        }

        public void setFormNumber(String formNumber) {
            this.formNumber = formNumber;
        }

        @JavascriptInterface
        public String getCrmsNumber() {
            return crmsNumber;
        }

        public void setCrmsNumber(String crmsNumber) {
            this.crmsNumber = crmsNumber;
        }

        @JavascriptInterface
        public String getApplicantName() {
            return applicantName;
        }

        public void setApplicantName(String applicantName) {
            this.applicantName = applicantName;
        }

        @JavascriptInterface
        public String getApplicantCnic() {
            return applicantCnic;
        }

        public void setApplicantCnic(String applicantCnic) {
            this.applicantCnic = applicantCnic;
        }

        @JavascriptInterface
        public String getRelation() {
            return relation;
        }

        public void setRelation(String relation) {
            this.relation = relation;
        }

        @JavascriptInterface

        public String getChildName() {
            return childName;
        }

        public void setChildName(String childName) {
            this.childName = childName;
        }

        @JavascriptInterface
        public String getFatherName() {
            return fatherName;
        }

        public void setFatherName(String fatherName) {
            this.fatherName = fatherName;
        }

        @JavascriptInterface
        public String getFatherCnic() {
            return fatherCnic;
        }

        public void setFatherCnic(String fatherCnic) {
            this.fatherCnic = fatherCnic;
        }

        @JavascriptInterface
        public String getMotherName() {
            return motherName;
        }

        public void setMotherName(String motherName) {
            this.motherName = motherName;
        }

        @JavascriptInterface
        public String getMotherCnic() {
            return motherCnic;
        }

        public void setMotherCnic(String motherCnic) {
            this.motherCnic = motherCnic;
        }

        @JavascriptInterface
        public String getGendor() {
            return gendor;
        }

        public void setGendor(String gendor) {
            this.gendor = gendor;
        }

        @JavascriptInterface
        public String getReligion() {
            return religion;
        }

        public void setReligion(String religion) {
            this.religion = religion;
        }

        @JavascriptInterface
        public String getDistrictOfBirth() {
            return districtOfBirth;
        }

        public void setDistrictOfBirth(String districtOfBirth) {
            this.districtOfBirth = districtOfBirth;
        }

        @JavascriptInterface
        public String getDateOfBirth() {
            return dateOfBirth;
        }

        public void setDateOfBirth(String dateOfBirth) {
            this.dateOfBirth = dateOfBirth;
        }

        @JavascriptInterface
        public String getGrandFatherName() {
            return grandFatherName;
        }

        public void setGrandFatherName(String grandFatherName) {
            this.grandFatherName = grandFatherName;
        }

        @JavascriptInterface
        public String getGrandFatherCnic() {
            return grandFatherCnic;
        }

        public void setGrandFatherCnic(String grandFatherCnic) {
            this.grandFatherCnic = grandFatherCnic;
        }

        @JavascriptInterface
        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        @JavascriptInterface
        public String getApplicantNameUrdu() {
            return applicantNameUrdu;
        }

        public void setApplicantNameUrdu(String applicantNameUrdu) {
            this.applicantNameUrdu = applicantNameUrdu;
        }

        @JavascriptInterface
        public String getApplicantCnicUrdu() {
            return applicantCnicUrdu;
        }

        public void setApplicantCnicUrdu(String applicantCnicUrdu) {
            this.applicantCnicUrdu = applicantCnicUrdu;
        }

        @JavascriptInterface
        public String getRelationUrdu() {
            return relationUrdu;
        }

        public void setRelationUrdu(String relationUrdu) {
            this.relationUrdu = relationUrdu;
        }

        @JavascriptInterface
        public String getChildNameUrdu() {
            return childNameUrdu;
        }

        public void setChildNameUrdu(String childNameUrdu) {
            this.childNameUrdu = childNameUrdu;
        }

        @JavascriptInterface

        public String getFatherNameUrdu() {
            return fatherNameUrdu;
        }

        public void setFatherNameUrdu(String fatherNameUrdu) {
            this.fatherNameUrdu = fatherNameUrdu;
        }

        @JavascriptInterface
        public String getFatherCnicUrdu() {
            return fatherCnicUrdu;
        }

        public void setFatherCnicUrdu(String fatherCnicUrdu) {
            this.fatherCnicUrdu = fatherCnicUrdu;
        }

        @JavascriptInterface
        public String getMotherNameUrdu() {
            return motherNameUrdu;
        }

        public void setMotherNameUrdu(String motherNameUrdu) {
            this.motherNameUrdu = motherNameUrdu;
        }

        @JavascriptInterface
        public String getMotherCnicUrdu() {
            return motherCnicUrdu;
        }

        public void setMotherCnicUrdu(String motherCnicUrdu) {
            this.motherCnicUrdu = motherCnicUrdu;
        }

        @JavascriptInterface
        public String getGendorUrdu() {
            return gendorUrdu;
        }

        public void setGendorUrdu(String gendorUrdu) {
            this.gendorUrdu = gendorUrdu;
        }

        @JavascriptInterface
        public String getReligionUrdu() {
            return religionUrdu;
        }

        public void setReligionUrdu(String religionUrdu) {
            this.religionUrdu = religionUrdu;
        }

        @JavascriptInterface
        public String getDistrictOfBirthUrdu() {
            return districtOfBirthUrdu;
        }

        public void setDistrictOfBirthUrdu(String districtOfBirthUrdu) {
            this.districtOfBirthUrdu = districtOfBirthUrdu;
        }

        @JavascriptInterface
        public String getDateOfBirthUrdu() {
            return dateOfBirthUrdu;
        }

        public void setDateOfBirthUrdu(String dateOfBirthUrdu) {
            this.dateOfBirthUrdu = dateOfBirthUrdu;
        }

        @JavascriptInterface
        public String getGrandFatherNameUrdu() {
            return grandFatherNameUrdu;
        }

        public void setGrandFatherNameUrdu(String grandFatherNameUrdu) {
            this.grandFatherNameUrdu = grandFatherNameUrdu;
        }

        @JavascriptInterface
        public String getGrandFatherCnicUrdu() {
            return grandFatherCnicUrdu;
        }

        public void setGrandFatherCnicUrdu(String grandFatherCnicUrdu) {
            this.grandFatherCnicUrdu = grandFatherCnicUrdu;
        }

        @JavascriptInterface
        public String getAddressUrdu() {
            return addressUrdu;
        }

        public void setAddressUrdu(String addressUrdu) {
            this.addressUrdu = addressUrdu;
        }

        @JavascriptInterface
        public String getEntryDate() {
            return entryDate;
        }

        public void setEntryDate(String entryDate) {
            this.entryDate = entryDate;
        }

        @JavascriptInterface
        public String getEntryType() {
            return entryType;
        }

        public void setEntryType(String entryType) {
            this.entryType = entryType;
        }

        @JavascriptInterface
        public String getIssueDateUrdu() {
            return issueDateUrdu;
        }

        public void setIssueDateUrdu(String issueDateUrdu) {
            this.issueDateUrdu = issueDateUrdu;
        }

        @JavascriptInterface
        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }



    @Override
    public Bundle getData() {

        Bundle bundle = new Bundle();

        bundle.putParcelable(NadraReportActivity.getNADRA_CARD(), this);
        return bundle;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.formNumber);
        dest.writeString(this.crmsNumber);
        dest.writeString(this.applicantName);
        dest.writeString(this.applicantCnic);
        dest.writeString(this.relation);
        dest.writeString(this.childName);
        dest.writeString(this.fatherName);
        dest.writeString(this.fatherCnic);
        dest.writeString(this.motherName);
        dest.writeString(this.motherCnic);
        dest.writeString(this.gendor);
        dest.writeString(this.religion);
        dest.writeString(this.districtOfBirth);
        dest.writeString(this.dateOfBirth);
        dest.writeString(this.grandFatherName);
        dest.writeString(this.grandFatherCnic);
        dest.writeString(this.address);
        dest.writeString(this.applicantNameUrdu);
        dest.writeString(this.applicantCnicUrdu);
        dest.writeString(this.relationUrdu);
        dest.writeString(this.childNameUrdu);
        dest.writeString(this.fatherNameUrdu);
        dest.writeString(this.fatherCnicUrdu);
        dest.writeString(this.motherNameUrdu);
        dest.writeString(this.motherCnicUrdu);
        dest.writeString(this.gendorUrdu);
        dest.writeString(this.religionUrdu);
        dest.writeString(this.districtOfBirthUrdu);
        dest.writeString(this.dateOfBirthUrdu);
        dest.writeString(this.grandFatherNameUrdu);
        dest.writeString(this.grandFatherCnicUrdu);
        dest.writeString(this.addressUrdu);
        dest.writeString(this.entryDate);
        dest.writeString(this.entryType);
        dest.writeString(this.issueDateUrdu);
        dest.writeString(this.imageUrl);
    }

    public BirthCertificateModel() {
    }

    protected BirthCertificateModel(Parcel in) {
        this.formNumber = in.readString();
        this.crmsNumber = in.readString();
        this.applicantName = in.readString();
        this.applicantCnic = in.readString();
        this.relation = in.readString();
        this.childName = in.readString();
        this.fatherName = in.readString();
        this.fatherCnic = in.readString();
        this.motherName = in.readString();
        this.motherCnic = in.readString();
        this.gendor = in.readString();
        this.religion = in.readString();
        this.districtOfBirth = in.readString();
        this.dateOfBirth = in.readString();
        this.grandFatherName = in.readString();
        this.grandFatherCnic = in.readString();
        this.address = in.readString();
        this.applicantNameUrdu = in.readString();
        this.applicantCnicUrdu = in.readString();
        this.relationUrdu = in.readString();
        this.childNameUrdu = in.readString();
        this.fatherNameUrdu = in.readString();
        this.fatherCnicUrdu = in.readString();
        this.motherNameUrdu = in.readString();
        this.motherCnicUrdu = in.readString();
        this.gendorUrdu = in.readString();
        this.religionUrdu = in.readString();
        this.districtOfBirthUrdu = in.readString();
        this.dateOfBirthUrdu = in.readString();
        this.grandFatherNameUrdu = in.readString();
        this.grandFatherCnicUrdu = in.readString();
        this.addressUrdu = in.readString();
        this.entryDate = in.readString();
        this.entryType = in.readString();
        this.issueDateUrdu = in.readString();
        this.imageUrl = in.readString();
    }

    public static final Creator<BirthCertificateModel> CREATOR = new Creator<BirthCertificateModel>() {
        @Override
        public BirthCertificateModel createFromParcel(Parcel source) {
            return new BirthCertificateModel(source);
        }

        @Override
        public BirthCertificateModel[] newArray(int size) {
            return new BirthCertificateModel[size];
        }
    };
}
