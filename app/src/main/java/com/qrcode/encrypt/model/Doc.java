package com.qrcode.encrypt.model;

import android.os.Bundle;
import android.os.Parcelable;

public interface Doc extends Parcelable {

    Bundle getData();
}
