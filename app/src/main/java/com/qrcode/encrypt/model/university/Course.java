package com.qrcode.encrypt.model.university;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Course implements Parcelable {

        @SerializedName("courseCode")
        @Expose
        private String courseCode;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("creditHours")
        @Expose
        private String creditHours;
        @SerializedName("maxMarks")
        @Expose
        private String maxMarks;
        @SerializedName("obtMarks")
        @Expose
        private String obtMarks;
        @SerializedName("cgpa")
        @Expose
        private String cgpa;
        @SerializedName("grade")
        @Expose
        private String grade;

        public String getCourseCode() {
            return courseCode;
        }

        public void setCourseCode(String courseCode) {
            this.courseCode = courseCode;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCreditHours() {
            return creditHours;
        }

        public void setCreditHours(String creditHours) {
            this.creditHours = creditHours;
        }

        public String getMaxMarks() {
            return maxMarks;
        }

        public void setMaxMarks(String maxMarks) {
            this.maxMarks = maxMarks;
        }

        public String getObtMarks() {
            return obtMarks;
        }

        public void setObtMarks(String obtMarks) {
            this.obtMarks = obtMarks;
        }

        public String getCgpa() {
            return cgpa;
        }

        public void setCgpa(String cgpa) {
            this.cgpa = cgpa;
        }

        public String getGrade() {
            return grade;
        }

        public void setGrade(String grade) {
            this.grade = grade;
        }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.courseCode);
        dest.writeString(this.name);
        dest.writeString(this.creditHours);
        dest.writeString(this.maxMarks);
        dest.writeString(this.obtMarks);
        dest.writeString(this.cgpa);
        dest.writeString(this.grade);
    }

    public Course() {
    }

    protected Course(Parcel in) {
        this.courseCode = in.readString();
        this.name = in.readString();
        this.creditHours = in.readString();
        this.maxMarks = in.readString();
        this.obtMarks = in.readString();
        this.cgpa = in.readString();
        this.grade = in.readString();
    }

    public static final Parcelable.Creator<Course> CREATOR = new Parcelable.Creator<Course>() {
        @Override
        public Course createFromParcel(Parcel source) {
            return new Course(source);
        }

        @Override
        public Course[] newArray(int size) {
            return new Course[size];
        }
    };
}
