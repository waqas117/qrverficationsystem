package com.qrcode.encrypt.model.uae;

import android.os.Bundle;
import android.os.Parcel;
import android.webkit.JavascriptInterface;

import com.qrcode.encrypt.activities.UaeReportActivity;
import com.qrcode.encrypt.model.Doc;

public class TitleDeedModel implements Doc{

    private String issueDate;
    private String propertyType;
    private String propertyTypeArabic;
    private String community;
    private String communityArabic;
    private String plotNo;
    private String buildingNo;
    private String buildingName;
    private String buildingNameArabic;
    private String propertyNo;
    private String floorNo;
    private String parking;
    private String areaSquareMeter;
    private String commonArea;
    private String owner;
    private String ownerArabic;
    private String share;
    private String purchaseDesc;
    private String purchaseDescArabic;


    @JavascriptInterface
    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    @JavascriptInterface
    public String getPropertyType() {
        return propertyType;
    }

    public void setPropertyType(String propertyType) {
        this.propertyType = propertyType;
    }

    @JavascriptInterface
    public String getPropertyTypeArabic() {
        return propertyTypeArabic;
    }

    public void setPropertyTypeArabic(String propertyTypeArabic) {
        this.propertyTypeArabic = propertyTypeArabic;
    }

    @JavascriptInterface
    public String getCommunity() {
        return community;
    }

    public void setCommunity(String community) {
        this.community = community;
    }

    @JavascriptInterface
    public String getCommunityArabic() {
        return communityArabic;
    }

    public void setCommunityArabic(String communityArabic) {
        this.communityArabic = communityArabic;
    }

    @JavascriptInterface
    public String getPlotNo() {
        return plotNo;
    }

    public void setPlotNo(String plotNo) {
        this.plotNo = plotNo;
    }

    @JavascriptInterface
    public String getBuildingNo() {
        return buildingNo;
    }

    public void setBuildingNo(String buildingNo) {
        this.buildingNo = buildingNo;
    }

    @JavascriptInterface
    public String getBuildingName() {
        return buildingName;
    }

    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }

    @JavascriptInterface
    public String getBuildingNameArabic() {
        return buildingNameArabic;
    }

    public void setBuildingNameArabic(String buildingNameArabic) {
        this.buildingNameArabic = buildingNameArabic;
    }

    @JavascriptInterface

    public String getPropertyNo() {
        return propertyNo;
    }

    public void setPropertyNo(String propertyNo) {
        this.propertyNo = propertyNo;
    }

    @JavascriptInterface
    public String getFloorNo() {
        return floorNo;
    }

    public void setFloorNo(String floorNo) {
        this.floorNo = floorNo;
    }

    @JavascriptInterface
    public String getParking() {
        return parking;
    }

    public void setParking(String parking) {
        this.parking = parking;
    }

    @JavascriptInterface
    public String getAreaSquareMeter() {
        return areaSquareMeter;
    }

    public void setAreaSquareMeter(String areaSquareMeter) {
        this.areaSquareMeter = areaSquareMeter;
    }

    @JavascriptInterface
    public String getCommonArea() {
        return commonArea;
    }

    public void setCommonArea(String commonArea) {
        this.commonArea = commonArea;
    }

    @JavascriptInterface
    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    @JavascriptInterface
    public String getShare() {
        return share;
    }

    public void setShare(String share) {
        this.share = share;
    }

    @JavascriptInterface
    public String getPurchaseDesc() {
        return purchaseDesc;
    }

    public void setPurchaseDesc(String purchaseDesc) {
        this.purchaseDesc = purchaseDesc;
    }

    @JavascriptInterface
    public String getPurchaseDescArabic() {
        return purchaseDescArabic;
    }

    public void setPurchaseDescArabic(String purchaseDescArabic) {
        this.purchaseDescArabic = purchaseDescArabic;
    }

    public String getOwnerArabic() {
        return ownerArabic;
    }

    public void setOwnerArabic(String ownerArabic) {
        this.ownerArabic = ownerArabic;
    }

    @Override
    public Bundle getData() {
        Bundle bundle = new Bundle();

        bundle.putParcelable(UaeReportActivity.getUAE_CARD(), this);
        return bundle;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.issueDate);
        dest.writeString(this.propertyType);
        dest.writeString(this.propertyTypeArabic);
        dest.writeString(this.community);
        dest.writeString(this.communityArabic);
        dest.writeString(this.plotNo);
        dest.writeString(this.buildingNo);
        dest.writeString(this.buildingName);
        dest.writeString(this.buildingNameArabic);
        dest.writeString(this.propertyNo);
        dest.writeString(this.floorNo);
        dest.writeString(this.parking);
        dest.writeString(this.areaSquareMeter);
        dest.writeString(this.commonArea);
        dest.writeString(this.owner);
        dest.writeString(this.ownerArabic);
        dest.writeString(this.share);
        dest.writeString(this.purchaseDesc);
        dest.writeString(this.purchaseDescArabic);
    }

    public TitleDeedModel() {
    }

    protected TitleDeedModel(Parcel in) {
        this.issueDate = in.readString();
        this.propertyType = in.readString();
        this.propertyTypeArabic = in.readString();
        this.community = in.readString();
        this.communityArabic = in.readString();
        this.plotNo = in.readString();
        this.buildingNo = in.readString();
        this.buildingName = in.readString();
        this.buildingNameArabic = in.readString();
        this.propertyNo = in.readString();
        this.floorNo = in.readString();
        this.parking = in.readString();
        this.areaSquareMeter = in.readString();
        this.commonArea = in.readString();
        this.owner = in.readString();
        this.ownerArabic =in.readString();
        this.share = in.readString();
        this.purchaseDesc = in.readString();
        this.purchaseDescArabic = in.readString();
    }

    public static final Creator<TitleDeedModel> CREATOR = new Creator<TitleDeedModel>() {
        @Override
        public TitleDeedModel createFromParcel(Parcel source) {
            return new TitleDeedModel(source);
        }

        @Override
        public TitleDeedModel[] newArray(int size) {
            return new TitleDeedModel[size];
        }
    };
}
