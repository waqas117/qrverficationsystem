/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qrcode.encrypt.model;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Atif
 */
public enum DocumentSubType {

    UNIVERSIT_TRANSCRIPT(11),
    UNIVERSITY_DEGREE(12),
    POLICE_LOST_REPORT(21),
    POLICE_FIR(22),
    BANK_NEW_ACCOUNT(31),
    BANK_STATEMENT_REQUEST(32),
    TRAVEL_HOTEL_RESERVATION(41),
    TRAVEL_SHIP_RESERVATION(42),
    NADRA_BIRTH_CERTIFICATE(51),
    NADRA_ID_CARD_REQUEST(52),
    UAE_BIRTH_CERTIFICATE(61),
    UAE_TITLE_DEED(62)
    ;

    private static final Map<Integer, DocumentSubType> documentSubTypeMap = new HashMap<>();

    static {
        for (DocumentSubType documentSubType : DocumentSubType.values()) {
            documentSubTypeMap.put(documentSubType.getCode(), documentSubType);
        }
    }

    private final int code;

    DocumentSubType(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public DocumentSubType toName(int code) {
        return documentSubTypeMap.get(code);
    }
}
