/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qrcode.encrypt.model;

/**
 *
 * @author Atif
 */
public class NewAccount {

    private String firstName;
    private String lastName;
    private String resedentialAddress;
    private String mailingAddress;
    private String email;
    private String dateOfBirth;
    private String phone;
    private String educationLevel;
    private String businessName;
    private String businessAddress;
    private String businessPhone;
    private String accountType;
    private String issueDate;
    private String imageUrl;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getResedentialAddress() {
        return resedentialAddress;
    }

    public void setResedentialAddress(String resedentialAddress) {
        this.resedentialAddress = resedentialAddress;
    }

    public String getMailingAddress() {
        return mailingAddress;
    }

    public void setMailingAddress(String mailingAddress) {
        this.mailingAddress = mailingAddress;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEducationLevel() {
        return educationLevel;
    }

    public void setEducationLevel(String educationLevel) {
        this.educationLevel = educationLevel;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
    }

    public String getBusinessPhone() {
        return businessPhone;
    }

    public void setBusinessPhone(String businessPhone) {
        this.businessPhone = businessPhone;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return "NewAccount{" + "firstName=" + firstName + ", lastName=" + lastName + ", resedentialAddress=" + resedentialAddress + ", mailingAddress=" + mailingAddress + ", email=" + email + ", dateOfBirth=" + dateOfBirth + ", phone=" + phone + ", educationLevel=" + educationLevel + ", businessName=" + businessName + ", businessAddress=" + businessAddress + ", businessPhone=" + businessPhone + ", accountType=" + accountType + ", issueDate=" + issueDate + ", imageUrl=" + imageUrl + '}';
    }
}
