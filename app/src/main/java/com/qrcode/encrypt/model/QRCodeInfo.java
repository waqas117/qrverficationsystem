/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qrcode.encrypt.model;

import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

/**
 *
 * @author Atif
 */


public class QRCodeInfo {


    @SerializedName("documentType")
    @Expose
    private Integer documentType;
    @SerializedName("documentSubType")
    @Expose
    private Integer documentSubType;
    @SerializedName("data")
    @Expose
    private JsonObject data;

    public Integer getDocumentType() {
        return documentType;
    }

    public void setDocumentType(Integer documentType) {
        this.documentType = documentType;
    }

    public Integer getDocumentSubType() {
        return documentSubType;
    }

    public void setDocumentSubType(Integer documentSubType) {
        this.documentSubType = documentSubType;
    }

    public JsonObject getData() {
        return data;
    }

    public void setData(JsonObject data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "QRCodeInfo{" + "documentType=" + documentType + ", documentSubType=" + documentSubType + ", data=" + data + '}';
    }
}
