package com.qrcode.encrypt.model.uae;

import android.os.Bundle;
import android.os.Parcel;
import android.webkit.JavascriptInterface;

import com.qrcode.encrypt.activities.UaeReportActivity;
import com.qrcode.encrypt.model.Doc;

public class UaeBirthCertificateModel implements Doc{
    private String district;
    private String name;
    private String sex;
    private String fatherName;
    private String fatherReligion;
    private String fatherNationality;
    private String motherName;
    private String motherReligion;
    private String motherNationality;
    private String dateOfBirth;
    private String placeOfBirth;
    private String dateOfIssue;
    private String registrationNumber;

    private String districtArabic;
    private String dateOfBirthArabic;

    @JavascriptInterface
    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    @JavascriptInterface
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JavascriptInterface
    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @JavascriptInterface
    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    @JavascriptInterface
    public String getFatherReligion() {
        return fatherReligion;
    }

    public void setFatherReligion(String fatherReligion) {
        this.fatherReligion = fatherReligion;
    }

    @JavascriptInterface
    public String getFatherNationality() {
        return fatherNationality;
    }

    public void setFatherNationality(String fatherNationality) {
        this.fatherNationality = fatherNationality;
    }

    @JavascriptInterface
    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    @JavascriptInterface

    public String getMotherReligion() {
        return motherReligion;
    }

    public void setMotherReligion(String motherReligion) {
        this.motherReligion = motherReligion;
    }

    @JavascriptInterface
    public String getMotherNationality() {
        return motherNationality;
    }

    public void setMotherNationality(String motherNationality) {
        this.motherNationality = motherNationality;
    }

    @JavascriptInterface
    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    @JavascriptInterface
    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    @JavascriptInterface
    public String getDateOfIssue() {
        return dateOfIssue;
    }

    public void setDateOfIssue(String dateOfIssue) {
        this.dateOfIssue = dateOfIssue;
    }

    @JavascriptInterface
    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    @JavascriptInterface
    public String getDistrictArabic() {
        return districtArabic;
    }

    public void setDistrictArabic(String districtArabic) {
        this.districtArabic = districtArabic;
    }

    @JavascriptInterface
    public String getDateOfBirthArabic() {
        return dateOfBirthArabic;
    }

    public void setDateOfBirthArabic(String dateOfBirthArabic) {
        this.dateOfBirthArabic = dateOfBirthArabic;
    }

    public static Creator<UaeBirthCertificateModel> getCREATOR() {
        return CREATOR;
    }

    @Override
    public Bundle getData() {

        Bundle bundle = new Bundle();

        bundle.putParcelable(UaeReportActivity.getUAE_CARD(), this);
        return bundle;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.district);
        dest.writeString(this.name);
        dest.writeString(this.sex);
        dest.writeString(this.fatherName);
        dest.writeString(this.fatherReligion);
        dest.writeString(this.fatherNationality);
        dest.writeString(this.motherName);
        dest.writeString(this.motherReligion);
        dest.writeString(this.motherNationality);
        dest.writeString(this.dateOfBirth);
        dest.writeString(this.placeOfBirth);
        dest.writeString(this.dateOfIssue);
        dest.writeString(this.registrationNumber);
        dest.writeString(this.districtArabic);
        dest.writeString(this.dateOfBirthArabic);
    }

    public UaeBirthCertificateModel() {
    }

    protected UaeBirthCertificateModel(Parcel in) {
        this.district = in.readString();
        this.name = in.readString();
        this.sex = in.readString();
        this.fatherName = in.readString();
        this.fatherReligion = in.readString();
        this.fatherNationality = in.readString();
        this.motherName = in.readString();
        this.motherReligion = in.readString();
        this.motherNationality = in.readString();
        this.dateOfBirth = in.readString();
        this.placeOfBirth = in.readString();
        this.dateOfIssue = in.readString();
        this.registrationNumber = in.readString();
        this.districtArabic = in.readString();
        this.dateOfBirthArabic = in.readString();
    }

    public static final Creator<UaeBirthCertificateModel> CREATOR = new Creator<UaeBirthCertificateModel>() {
        @Override
        public UaeBirthCertificateModel createFromParcel(Parcel source) {
            return new UaeBirthCertificateModel(source);
        }

        @Override
        public UaeBirthCertificateModel[] newArray(int size) {
            return new UaeBirthCertificateModel[size];
        }
    };
}
