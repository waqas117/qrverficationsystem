/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qrcode.encrypt.model;

/**
 *
 * @author Atif
 */
public class LostReport {

    private String name;
    private String fatherName;
    private String cnic;
    private String address;
    private String phone;
    private String email;
    private String placeOfOccurance;
    private String district;
    private String policeStation;
    private String fromDate;
    private String toDate;
    private String complainDetail;
    private String issueDate;
    private String imageUrl;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getCnic() {
        return cnic;
    }

    public void setCnic(String cnic) {
        this.cnic = cnic;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPlaceOfOccurance() {
        return placeOfOccurance;
    }

    public void setPlaceOfOccurance(String placeOfOccurance) {
        this.placeOfOccurance = placeOfOccurance;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getPoliceStation() {
        return policeStation;
    }

    public void setPoliceStation(String policeStation) {
        this.policeStation = policeStation;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getComplainDetail() {
        return complainDetail;
    }

    public void setComplainDetail(String complainDetail) {
        this.complainDetail = complainDetail;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return "LostReport{" + "name=" + name + ", fatherName=" + fatherName + ", cnic=" + cnic + ", address=" + address + ", phone=" + phone + ", email=" + email + ", placeOfOccurance=" + placeOfOccurance + ", district=" + district + ", policeStation=" + policeStation + ", fromDate=" + fromDate + ", toDate=" + toDate + ", complainDetail=" + complainDetail + ", issueDate=" + issueDate + ", imageUrl=" + imageUrl + '}';
    }
}
