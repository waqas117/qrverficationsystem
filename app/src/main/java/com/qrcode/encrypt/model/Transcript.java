/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qrcode.encrypt.model;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.charset.Charset;

/**
 *
 * @author Atif
 */
public class Transcript implements Serializable {

    private static final long serialVersionUID = 1357924680L;

    private String universityName;
    private String address;
    private String documentType;
    private String programName;
    private String programSession;
    private String candidateName;
    private String rollNumber;
    private String fatherName;
    private String registrationNumber;
    private String comprehensiveExamNotificationNumber;
    private String comprehensiveExamNotificationDate;
    private String comprehensiveExamRollNumber;
    private String comprehensiveExamResult;
    private String totalMarks;
    private String obtainMarks;
    private String cgpa;
    private String comulativeGrade;
    private String percentage;
    private String issueDate;
    private String qrCodeImageUrl;

    public String getUniversityName() {
        return universityName;
    }

    public void setUniversityName(String universityName) {
        this.universityName = universityName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public String getProgramSession() {
        return programSession;
    }

    public void setProgramSession(String programSession) {
        this.programSession = programSession;
    }

    public String getCandidateName() {
        return candidateName;
    }

    public void setCandidateName(String candidateName) {
        this.candidateName = candidateName;
    }

    public String getRollNumber() {
        return rollNumber;
    }

    public void setRollNumber(String rollNumber) {
        this.rollNumber = rollNumber;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getComprehensiveExamNotificationNumber() {
        return comprehensiveExamNotificationNumber;
    }

    public void setComprehensiveExamNotificationNumber(String comprehensiveExamNotificationNumber) {
        this.comprehensiveExamNotificationNumber = comprehensiveExamNotificationNumber;
    }

    public String getComprehensiveExamNotificationDate() {
        return comprehensiveExamNotificationDate;
    }

    public void setComprehensiveExamNotificationDate(String comprehensiveExamNotificationDate) {
        this.comprehensiveExamNotificationDate = comprehensiveExamNotificationDate;
    }

    public String getComprehensiveExamRollNumber() {
        return comprehensiveExamRollNumber;
    }

    public void setComprehensiveExamRollNumber(String comprehensiveExamRollNumber) {
        this.comprehensiveExamRollNumber = comprehensiveExamRollNumber;
    }

    public String getComprehensiveExamResult() {
        return comprehensiveExamResult;
    }

    public void setComprehensiveExamResult(String comprehensiveExamResult) {
        this.comprehensiveExamResult = comprehensiveExamResult;
    }

    public String getTotalMarks() {
        return totalMarks;
    }

    public void setTotalMarks(String totalMarks) {
        this.totalMarks = totalMarks;
    }

    public String getObtainMarks() {
        return obtainMarks;
    }

    public void setObtainMarks(String obtainMarks) {
        this.obtainMarks = obtainMarks;
    }

    public String getCgpa() {
        return cgpa;
    }

    public void setCgpa(String cgpa) {
        this.cgpa = cgpa;
    }

    public String getComulativeGrade() {
        return comulativeGrade;
    }

    public void setComulativeGrade(String comulativeGrade) {
        this.comulativeGrade = comulativeGrade;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getQrCodeImageUrl() {
        return qrCodeImageUrl;
    }

    public void setQrCodeImageUrl(String qrCodeImageUrl) {
        this.qrCodeImageUrl = qrCodeImageUrl;
    }

    @Override
    public String toString() {
        return "UniversityTranscript{" + "universityName=" + universityName + ", address=" + address + ", documentType=" + documentType + ", programName=" + programName + ", programSession=" + programSession + ", candidateName=" + candidateName + ", rollNumber=" + rollNumber + ", fatherName=" + fatherName + ", registrationNumber=" + registrationNumber + ", comprehensiveExamNotificationNumber=" + comprehensiveExamNotificationNumber + ", comprehensiveExamNotificationDate=" + comprehensiveExamNotificationDate + ", comprehensiveExamRollNumber=" + comprehensiveExamRollNumber + ", comprehensiveExamResult=" + comprehensiveExamResult + ", totalMarks=" + totalMarks + ", obtainMarks=" + obtainMarks + ", cgpa=" + cgpa + ", comulativeGrade=" + comulativeGrade + ", percentage=" + percentage + ", issueDate=" + issueDate + ", qrCodeImageUrl=" + qrCodeImageUrl + '}';
    }

    public byte[] getSerializeObjectArray() throws IOException {
        //Serialization of object
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream out = new ObjectOutputStream(bos);
        out.writeObject(this);
        return bos.toByteArray();
    }

    public String getSerializeObjectString() throws IOException {
        //Serialization of object
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream out = new ObjectOutputStream(bos);
        out.writeObject(this);
        return bos.toString("UTF-8");
    }

    public Transcript getDeSerializeObject(byte[] object) throws IOException, ClassNotFoundException {
        //De-serialization of object
        ByteArrayInputStream bis = new ByteArrayInputStream(object);
        ObjectInputStream in = new ObjectInputStream(bis);
        Transcript transcript = (Transcript) in.readObject();
        return transcript;
    }

    public Transcript getDeSerializeObject(String object) throws IOException, ClassNotFoundException {
        //De-serialization of object
        ByteArrayInputStream bis = new ByteArrayInputStream(object.getBytes(Charset.forName("UTF-8")));
        ObjectInputStream in = new ObjectInputStream(bis);
        Transcript transcript = (Transcript) in.readObject();
        return transcript;
    }
}
