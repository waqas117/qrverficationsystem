package com.qrcode.encrypt.model.university;


import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.webkit.JavascriptInterface;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.qrcode.encrypt.activities.UniversityActivity;
import com.qrcode.encrypt.model.Doc;

public class UniversityDegree implements Doc {

    @SerializedName("studentName")
    @Expose
    private String studentName;
    @SerializedName("degreeName")
    @Expose
    private String degreeName;
    @SerializedName("timeSpan")
    @Expose
    private String timeSpan;
    @SerializedName("issueDate")
    @Expose
    private String issueDate;
    @SerializedName("qrCodeImageUrl")
    @Expose
    private String qrCodeImageUrl;

    @JavascriptInterface
    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    @JavascriptInterface
    public String getDegreeName() {
        return degreeName;
    }

    public void setDegreeName(String degreeName) {
        this.degreeName = degreeName;
    }

    @JavascriptInterface
    public String getTimeSpan() {
        return timeSpan;
    }

    public void setTimeSpan(String timeSpan) {
        this.timeSpan = timeSpan;
    }

    @JavascriptInterface
    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    @JavascriptInterface
    public String getQrCodeImageUrl() {
        return qrCodeImageUrl;
    }

    public void setQrCodeImageUrl(String qrCodeImageUrl) {
        this.qrCodeImageUrl = qrCodeImageUrl;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.studentName);
        dest.writeString(this.degreeName);
        dest.writeString(this.timeSpan);
        dest.writeString(this.issueDate);
        dest.writeString(this.qrCodeImageUrl);
    }

    public UniversityDegree() {
    }

    protected UniversityDegree(Parcel in) {
        this.studentName = in.readString();
        this.degreeName = in.readString();
        this.timeSpan = in.readString();
        this.issueDate = in.readString();
        this.qrCodeImageUrl = in.readString();
    }

    public static final Parcelable.Creator<UniversityDegree> CREATOR = new Parcelable.Creator<UniversityDegree>() {
        @Override
        public UniversityDegree createFromParcel(Parcel source) {
            return new UniversityDegree(source);
        }

        @Override
        public UniversityDegree[] newArray(int size) {
            return new UniversityDegree[size];
        }
    };

    @Override
    public Bundle getData() {

        Bundle bundle = new Bundle();

        bundle.putParcelable(UniversityActivity.getUNIVERSITY_CARD(), this);
        return bundle;
    }
}
